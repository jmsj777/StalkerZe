package com.example.jmsj.stalkerze.view;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;
import com.example.jmsj.stalkerze.R;
import com.example.jmsj.stalkerze.model.Victim;

public class VictimsListViewHolder extends RecyclerView.ViewHolder
                                    implements View.OnClickListener{

    private TextView textViewFirstName;
    private TextView textViewLastName;
    private TextView textViewAge;
    private ImageView imageViewPhoto;

    private Victim victim;
    private VictimsListAdapter.VictimListener listener;

    public VictimsListViewHolder(@NonNull View itemView,
                                 VictimsListAdapter.VictimListener listener) {
        super(itemView);
        textViewFirstName = itemView.findViewById(R.id.textViewFirstName);
        textViewLastName = itemView.findViewById(R.id.textViewLastName);
        textViewAge = itemView.findViewById(R.id.textViewAge);
        imageViewPhoto = itemView.findViewById(R.id.imageViewPhoto);
        this.listener = listener;
        itemView.setOnClickListener(this);
    }

    public void bind(Victim victim){
        textViewFirstName.setText(victim.getFirstName());
        textViewLastName.setText(victim.getLastName());
        textViewAge.setText(Integer.toString(victim.getAge()));
        imageViewPhoto.setImageBitmap(victim.getPhotos().get(0));
        this.victim = victim;
    }

    @Override
    public void onClick(View v) {
        listener.onClickVictimListener(this.victim);
    }
}
