package com.example.jmsj.stalkerze.data;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

class DAOHelper<T> {

    private File path;
    private String fileName;
    private File file;

    DAOHelper(File path, String fileName) {
        this.path = path;
        this.fileName = fileName;
        this.file = new File(this.path, this.fileName);
    }

    void setPath(File path) {
        this.path = path;
        this.file = new File(this.path, this.fileName);
    }

    void setFileName(String fileName) {
        this.fileName = fileName;
        this.file = new File(this.path, this.fileName);
    }

    void saveObject(T object) throws FileNotFoundException, IOException {
        FileOutputStream fos = new FileOutputStream(this.file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.close();
        fos.close();
    }

    T loadObject() throws FileNotFoundException, IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(this.file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        T object = (T) ois.readObject();
        ois.close();
        fis.close();
        return object;
    }
}
