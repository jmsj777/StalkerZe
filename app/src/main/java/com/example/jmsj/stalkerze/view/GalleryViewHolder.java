package com.example.jmsj.stalkerze.view;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.jmsj.stalkerze.R;

public class GalleryViewHolder extends RecyclerView.ViewHolder
                                implements View.OnClickListener{

    private ImageView imgPic;
    private GalleryAdapter.GalleryListener galleryListener;
    private Bitmap pic;
    private static final int color = Color.parseColor("#80000000");

    public GalleryViewHolder(@NonNull View itemView,
                             GalleryAdapter.GalleryListener galleryListener) {
        super(itemView);
        imgPic = itemView.findViewById(R.id.imgPic);
        itemView.setOnClickListener(this);
        this.galleryListener = galleryListener;
    }

    public void bind(Bitmap pic){
        this.pic = pic;
        this.imgPic.setImageBitmap(this.pic);
    }

    @Override
    public void onClick(View v) {
        galleryListener.onClickPhoto(this.pic);
    }

}
