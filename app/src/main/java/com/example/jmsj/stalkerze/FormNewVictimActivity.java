package com.example.jmsj.stalkerze;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.jmsj.stalkerze.data.DAOVictim;
import com.example.jmsj.stalkerze.model.Victim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class FormNewVictimActivity extends AppCompatActivity {

    private static int REQUEST_PHOTOS_FORM_CODE = 1111;

    private String firstName;
    private String lastName ;
    private Integer age;
    private String job;
    private String birthday;
    private String phone ;
    private String description;
    private ArrayList<Bitmap> photos;

    private Toast infoToast;
    private ImageButton btTakePic;

    private LinearLayout linearLayoutAddPics;
    public static final String PHOTOS_KEY = "com.example.jmsj.stalkerze.FormNewVictimActivity.PHOTOS";

    public static final String FILENAME = "messages.obj";
    private static final int REQUEST_PERMISSION_CODE = 1234;
    private File path;
    private static final String[] REQUEST_PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_new_victim);

        linearLayoutAddPics = findViewById(R.id.linearLayoutAddPics);

        btTakePic = findViewById(R.id.btTakePic);
        photos = new ArrayList<>();

        verifyPermissions(this);
        this.path = new File(Environment.getExternalStorageDirectory(), "testfile/history");
        DAOVictim.getINSTANCE().setFilePath(this.path, FILENAME);
    }

    public void onClickAddPics(View view){
        Intent intent = new Intent(getBaseContext(), PhotosFormActivity.class);
        intent.putExtra(PHOTOS_KEY, photos);
        startActivityForResult(intent, REQUEST_PHOTOS_FORM_CODE);
    }

    public void onClickSave(View view){

        firstName = ((EditText)findViewById(R.id.editTextFirstName)).getText().toString();
        lastName = ((EditText)findViewById(R.id.editTextLastName)).getText().toString();
        try {
            age = Integer.parseInt(
                    ((EditText) findViewById(R.id.editTextAge))
                            .getText().toString());
        }
        catch (NumberFormatException e) {
            infoToast = Toast.makeText(getBaseContext(), getString(R.string.wrong_age), Toast.LENGTH_SHORT);
            infoToast.show();
            age = 0;
            finish();
        }
        job = ((EditText)findViewById(R.id.editTextJob)).getText().toString();
        birthday = ((EditText)findViewById(R.id.editTextBirthday)).getText().toString();
        phone = ((EditText)findViewById(R.id.editTextPhone)).getText().toString();
        description = ((EditText)findViewById(R.id.editTextDescription)).getText().toString();

//        try{
//            photos = ((BitmapDrawable)LinearLayout.getDrawable()).getBitmap();
//        }
//            catch  (NullPointerException e){
/*
                infoToast = Toast.makeText(getBaseContext(), "ERROR! You have not provided an image!" , Toast.LENGTH_SHORT);
                infoToast.show();
                finish();
*/
//        }

        /*infoToast = Toast.makeText(getBaseContext(), Integer.toString(age) , Toast.LENGTH_SHORT);
        infoToast.show();*/

        Victim victim = new Victim(
                firstName,
                lastName,
                age,
                job,
                birthday,
                phone,
                description,
                photos
        );

        try {
            DAOVictim.getINSTANCE().addVictim(victim);
        } catch (FileNotFoundException e) {
            Toast.makeText(this, "File problem: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "Write problem: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ClassNotFoundException e) {
            Toast.makeText(this, "Class problem: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        infoToast = Toast.makeText(getBaseContext(), "Saved Successfully!" , Toast.LENGTH_SHORT);
        infoToast.show();
        finish();
    }

    public static final void verifyPermissions(Activity activity){
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                    activity,
                    REQUEST_PERMISSIONS,
                    REQUEST_PERMISSION_CODE
            );
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_PHOTOS_FORM_CODE && resultCode == RESULT_OK && data != null) {
            photos = data.getParcelableArrayListExtra(PhotosFormActivity.LISTPICS_KEY);
            btTakePic.setImageBitmap(photos.get(0));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_PERMISSION_CODE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if(!this.path.exists() && !this.path.mkdirs()) {
                    Toast.makeText(this, "Failed to create path", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
