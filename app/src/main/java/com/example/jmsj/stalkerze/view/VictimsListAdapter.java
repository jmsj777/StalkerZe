package com.example.jmsj.stalkerze.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.jmsj.stalkerze.ListVictimsActivity;
import com.example.jmsj.stalkerze.R;
import com.example.jmsj.stalkerze.data.DAOVictim;
import com.example.jmsj.stalkerze.model.Victim;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class VictimsListAdapter extends  RecyclerView.Adapter<VictimsListViewHolder> {
    private ArrayList<Victim> victimsList;
    private int countCreate;
    private VictimListener listener;

    public VictimsListAdapter(VictimListener listener) {
        try {
            victimsList = DAOVictim.getINSTANCE().getVictims();
        } catch (FileNotFoundException e) {
            //Toast.makeText(this, "File problem: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            //Toast.makeText(this, "IO problem: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ClassNotFoundException e) {
            //Toast.makeText(this, "Class problem: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        countCreate = 0;
        this.listener = listener;
    }

    public interface VictimListener {
        void onClickVictimListener(Victim v);
    }

    @NonNull
    @Override
    public VictimsListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.victim_resume, viewGroup, false);
        VictimsListViewHolder peopleListViewHolder = new VictimsListViewHolder(view, this.listener);
        countCreate++;
        Log.d("TESTEPPLACRIACAODEVH", Integer.toString(countCreate));
        return peopleListViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VictimsListViewHolder victimsListViewHolder, int i) {
        victimsListViewHolder.bind(victimsList.get(i));
        Log.d("TESTEPPLABINDS", Integer.toString(i));
    }

    @Override
    public int getItemCount() {
        return victimsList.size();
    }
}
