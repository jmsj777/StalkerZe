package com.example.jmsj.stalkerze;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jmsj.stalkerze.model.Victim;

public class ShowVictimActivity extends AppCompatActivity {

    TextView textViewFNR;
    TextView textViewLNR;
    TextView textViewAR;
    TextView textViewJR;
    TextView textViewBR;
    TextView textViewPR;
    TextView textViewDR;
    ImageView imageViewPhoto;

    Victim victim;
    public static final String LISTPICS_KEY = "com.example.jmsj.stalkerze.ShowVictimActivity.LISTPICS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_victim);

        textViewFNR = (TextView) findViewById(R.id.textViewFNR);
        textViewLNR = (TextView) findViewById(R.id.textViewLNR);
        textViewAR = (TextView) findViewById(R.id.textViewAR);
        textViewJR = (TextView) findViewById(R.id.textViewJR);
        textViewBR = (TextView) findViewById(R.id.textViewBR);
        textViewPR = (TextView) findViewById(R.id.textViewPR);
        textViewDR = (TextView) findViewById(R.id.textViewDR);
        imageViewPhoto = (ImageView) findViewById(R.id.imageViewPhoto);

        victim = getIntent().getParcelableExtra(ListVictimsActivity.VICTIM_KEY);

        textViewFNR.setText(victim.getFirstName());
        textViewLNR.setText(victim.getLastName());
        textViewAR.setText(Integer.toString(victim.getAge()));
        textViewJR.setText(victim.getJob());
        textViewBR.setText(victim.getBirthday());
        textViewPR.setText(victim.getPhone());
        textViewDR.setText(victim.getDescription());
        imageViewPhoto.setImageBitmap(victim.getPhotos().get(0));
    }

    public void onClickSeeVictimGallery(View view){
        Intent intent = new Intent(this, GalleryVictimActivity.class);
        intent.putExtra(LISTPICS_KEY, victim.getPhotos());
        startActivity(intent);
    }
}
