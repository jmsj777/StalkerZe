package com.example.jmsj.stalkerze;

import android.graphics.Bitmap;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.BoringLayout;
import android.util.DisplayMetrics;
import android.view.Display;

import com.example.jmsj.stalkerze.view.GalleryAdapter;
import com.example.jmsj.stalkerze.view.PhotoOpenDialogFragment;

import java.util.ArrayList;

public class GalleryFormActivity extends AppCompatActivity
    implements GalleryAdapter.GalleryListener {

    private RecyclerView rvGallery;
    private GalleryAdapter galleryAdapter;
    private PhotoOpenDialogFragment photoOpenDialogFragment;
    ArrayList<Bitmap> picsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_form);

        rvGallery = (RecyclerView) findViewById(R.id.rvGallery);
        picsList = getIntent().getParcelableArrayListExtra(PhotosFormActivity.LISTPICS_KEY);

        galleryAdapter = new GalleryAdapter(picsList, this);

        photoOpenDialogFragment = new PhotoOpenDialogFragment();

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int spanCount = (int) java.lang.Math.ceil(dpWidth / 100.0);

        rvGallery.setLayoutManager(new GridLayoutManager(this, spanCount));
        rvGallery.setHasFixedSize(true);
        rvGallery.setAdapter(galleryAdapter);
    }
    @Override
    public void onClickPhoto(Bitmap picture) {
        photoOpenDialogFragment.setBitmap(picture);
        FragmentManager fragmentManager = getSupportFragmentManager();
        photoOpenDialogFragment.show(fragmentManager, "showpic");
    }

}
