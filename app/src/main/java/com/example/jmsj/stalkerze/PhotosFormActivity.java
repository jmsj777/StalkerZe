package com.example.jmsj.stalkerze;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class PhotosFormActivity extends AppCompatActivity {

    private static int REQUEST_CAMERA_ACTIVITY_CODE = 2222;
    public static final String LISTPICS_KEY = "com.example.jmsj.buddyfinderze.PhotosFormActivity.LISTPICS";
    ArrayList<Bitmap> listPics;

    Toast infoToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_form);
        listPics = new ArrayList<>();
    }

    public void onClickTakePics(View view) {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivityForResult(intent, REQUEST_CAMERA_ACTIVITY_CODE);
    }

    public void onClickSeeGallery(View view) {
        Intent intent = new Intent(this, GalleryFormActivity.class);
        intent.putExtra(LISTPICS_KEY, listPics);
        intent.putExtra("ImInForm", false);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode==REQUEST_CAMERA_ACTIVITY_CODE && resultCode == RESULT_OK){
            Bitmap bitmap = data.getParcelableExtra(CameraActivity.BITMAP_KEY);
            listPics.add(bitmap);
            infoToast = Toast.makeText(getBaseContext(), "salvou imagem" , Toast.LENGTH_SHORT);
            infoToast.show();
        }
    }

    public  void onClickReturn(View view) {
        Intent intent = new Intent();
        infoToast = Toast.makeText(getBaseContext(), "cheguei aqui 11111" , Toast.LENGTH_LONG);
        infoToast.show();
        intent.putExtra(LISTPICS_KEY, listPics);
        setResult(RESULT_OK, intent);
        infoToast = Toast.makeText(getBaseContext(), "cheguei aqui" , Toast.LENGTH_SHORT);
        infoToast.show();
        finish();
    }


}

