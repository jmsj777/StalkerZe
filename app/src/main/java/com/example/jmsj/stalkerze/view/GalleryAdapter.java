package com.example.jmsj.stalkerze.view;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jmsj.stalkerze.R;

import java.util.ArrayList;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryViewHolder> {

    private ArrayList<Bitmap> picList;
    private GalleryListener galleryListener;


    public GalleryAdapter(ArrayList<Bitmap> picList, GalleryListener galleryListener) {
        this.picList = picList;
        this.galleryListener = galleryListener;
    }

    public interface GalleryListener{
        void onClickPhoto(Bitmap pic);
    }

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.view_holder_gallery, viewGroup, false);
        GalleryViewHolder galleryViewHolder = new GalleryViewHolder(view, galleryListener);
        return galleryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder galleryViewHolder, int i) {
        galleryViewHolder.bind(this.picList.get(i));
    }

    @Override
    public int getItemCount() {
        return this.picList.size();
    }

    public void setPicList(ArrayList<Bitmap> picList) {
        this.picList = picList;
    }

}
