package com.example.jmsj.stalkerze.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Victim implements Parcelable {

    private String firstName;
    private String lastName;
    private Integer age;
    private String job;
    private String birthday;
    private String phone;
    private String description;
    private ArrayList<Bitmap> photos;


    protected Victim(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        if (in.readByte() == 0) {
            age = null;
        } else {
            age = in.readInt();
        }
        job = in.readString();
        birthday = in.readString();
        phone = in.readString();
        description = in.readString();
        photos = in.createTypedArrayList(Bitmap.CREATOR);
    }

    public static final Creator<Victim> CREATOR = new Creator<Victim>() {
        @Override
        public Victim createFromParcel(Parcel in) {
            return new Victim(in);
        }

        @Override
        public Victim[] newArray(int size) {
            return new Victim[size];
        }
    };

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }

    public String getJob() {
        return job;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<Bitmap> getPhotos() {
        return photos;
    }

    public Victim(String firstName, String lastName, Integer age, String job, String birthday, String phone, String description, ArrayList<Bitmap> photos) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.job = job;
        this.birthday = birthday;
        this.phone = phone;
        this.description = description;
        this.photos = photos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        if (age == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(age);
        }
        dest.writeString(job);
        dest.writeString(birthday);
        dest.writeString(phone);
        dest.writeString(description);
        dest.writeTypedList(photos);
    }
}
